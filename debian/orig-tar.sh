#/bin/sh -e

VERSION=$2
TAR=../stapler-adjunct-timeline_$VERSION.orig.tar.gz
DIR=stapler-adjunct-timeline-$VERSION
mkdir -p $DIR

# Unpack ready fo re-packing
tar -xzf $TAR -C $DIR --strip-components=1

# Repack excluding stuff we don't need
GZIP=--best tar -czf $TAR --exclude 'timeline_js' --exclude 'timeline_ajax' \
     --exclude 'CVS' --exclude '.svn' --exclude 'debian' --exclude 'src/test' \
     $DIR
rm -rf $DIR

